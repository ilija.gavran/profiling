# Profiling

Prerequisites:
*  CMake (version >= 3.1)
*  Visual Studio 2017
    * if you're using other version, edit the CMake generator in [scripts/VSUpdate_.bat](scripts/VSUpdate_.bat)

Getting Started
* clone repository
    * e.g.  `git clone --recurse-submodules https://gitlab.com/ilija.gavran/profiling.git .`
* run [VSUpdateAndOpenSolution.bat](VSUpdateAndOpenSolution.bat)
