
#include <performance/profiler.h>
#include <performance_gui/main_window.h>

#include <atomic>
#include <chrono>
#include <functional>
#include <thread>

static void MyTimedFunction()
{
  TIMED_FUNCTION;

  using namespace std::chrono;

  {
    TIMED_BLOCK("I");
    std::this_thread::sleep_for(milliseconds(5));
  }

  for (int i = 0; i < 10; ++i)
  {
    TIMED_BLOCK("II");

    {
      TIMED_BLOCK("II-1");
      std::this_thread::sleep_for(milliseconds(13));
    }

    {
      TIMED_BLOCK("II-2");
      std::this_thread::sleep_for(milliseconds(21));
    }

    std::this_thread::sleep_for(milliseconds(7));
  }

  std::this_thread::sleep_for(microseconds(2));
}

namespace {

struct cleanup_t
{
  template<typename T>
  cleanup_t(T t) : fnc_(t) {}
  ~cleanup_t() { fnc_(); }

  std::function<void(void)> fnc_;
};

}

static int real_main()
{
  std::atomic_bool running(true);

  MyTimedFunction();

  std::thread thread_one([&running]() {
    while (running.load(std::memory_order_relaxed)) {
      MyTimedFunction();
    }
  });
  std::thread thread_two([&running]() {
    while (running.load(std::memory_order_relaxed)) {
      TIMED_BLOCK("Lambda");
      MyTimedFunction();
      MyTimedFunction();
    }
  });

  cleanup_t cleanup([&running, &thread_one, &thread_two]() {
    running.store(false, std::memory_order_relaxed);

    thread_one.join();
    thread_two.join();
  });

  performance::gui::main_window_options options;
  return performance::gui::main_window("Example", options);
}

#include <Windows.h>
int WinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, PSTR /*lpCmdLine*/, int /*nCmdShow*/)
{
  try {
    return real_main();
  } catch (std::exception& ex) {
    MessageBoxA(NULL, ex.what(), "Exception", MB_ICONERROR | MB_OK);
  } catch (...) {
    MessageBoxA(NULL, "Unrecognized exception type.", "Exception", MB_ICONERROR | MB_OK);
  }
  return EXIT_FAILURE;
}
