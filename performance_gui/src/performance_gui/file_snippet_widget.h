
#ifndef FILE_SNIPPET_WIDGET_H_INCLUDED_99C227FB_9365_46DD_BA12_4957AFFD1E3B
#define FILE_SNIPPET_WIDGET_H_INCLUDED_99C227FB_9365_46DD_BA12_4957AFFD1E3B

#include <string>
#include <memory>
#include <mutex>

namespace performance {
namespace gui {

class file_snippet_widget : public std::enable_shared_from_this<file_snippet_widget>
{
public:
  file_snippet_widget() = default;
  ~file_snippet_widget() = default;
public:
  void load_and_show(const char* path, int line);

  void render();

private:
  void load_and_show_(const char* path, int line);
private:
  bool is_visible = false;
  std::string file_path;
  int file_line = -1;
  std::string file_contents;
  mutable std::mutex mtx;
};

}
}

#endif // FILE_SNIPPET_WIDGET_H_INCLUDED_99C227FB_9365_46DD_BA12_4957AFFD1E3B
