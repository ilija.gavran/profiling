
#include <performance_gui/file_snippet_widget.h>

#include <imgui.h>

#include <performance/profiler.h>

#include <fstream>
#include <future>
#include <sstream>

namespace {

int get_num_digits(int number)
{
  int result = 1;
  while (number > 9) {
    number /= 10;
    ++result;
  }
  return result;
}

std::string get_file_contents(const char* file_path, int line)
{
  TIMED_FUNCTION;

  constexpr int line_radius = 7;
  const int line_max_digits = get_num_digits(line + line_radius);

  auto buffer = std::make_unique<char[]>(line_max_digits + 1);
  for (int i = 0; i < line_max_digits; ++i) {
    buffer[i] = ' ';
  }

  std::stringstream result;

  int current_line = 0;
  std::string str_line;

  std::ifstream ifs(file_path);
  while (!ifs.eof() && (current_line < line + line_radius)) {
    std::getline(ifs, str_line);

    ++current_line;

    if (std::abs(current_line - line) <= line_radius) {
      // print current line number to buffer
      {
        char* target = buffer.get() + line_max_digits - 1;
        for (auto temp_number = current_line; temp_number > 0; temp_number /= 10) {
          *target-- = '0' + (temp_number % 10);
        }
      }

      result << buffer << ": " << str_line << "\n";
    }
  }

  return result.str();
}

}

namespace performance {
namespace gui {

void file_snippet_widget::load_and_show(const char* path, int line)
{
  auto f = std::async([shared_this = shared_from_this(), path, line]() {
    shared_this->load_and_show_(path, line);
  });
}

void file_snippet_widget::load_and_show_(const char* path, int line)
{
  std::unique_lock<std::mutex> guard(mtx);
  if (file_line == line && !std::strcmp(path, file_path.c_str())) {
    is_visible = true;
    return;
  }
  guard.unlock();
  auto contents = get_file_contents(path, line);
  guard.lock();
  file_path = path;
  file_line = line;
  file_contents = std::move(contents);
  is_visible = true;
}

void file_snippet_widget::render()
{
  std::lock_guard<std::mutex> guard(mtx);
  if (!is_visible) {
    return;
  }
  ImGui::Begin(file_path.c_str(), &is_visible);
  if (file_contents.size() > 0) {
    ImGui::Text(file_contents.c_str());
  }
  ImGui::End();
}

}
}
