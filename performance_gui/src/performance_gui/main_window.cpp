
#include <performance_gui/main_window.h>

#include <performance_gui/file_snippet_widget.h>

#include <imgui.h>
#include <imgui_win32dx11/imgui_app.h>

#include <performance/profiler.h>
#include <performance/debug_stats.h>

#include <memory>
#include <sstream>

namespace performance {
namespace gui {

int main_window(const char* title, const main_window_options& options)
{
  debug_stats* stats = nullptr;

  const bool hide_source = options.hide_source;

  std::shared_ptr<file_snippet_widget> file_snippet;
  if (!hide_source) {
    file_snippet = std::make_shared<file_snippet_widget>();
  }

  debug_stats_options ui_options;
  ui_options.timing = debug_stats_timing::inclusive;
  ui_options.stat_type = debug_stats_type::none;
  ui_options.unit = debug_stats_unit::nanoseconds;
  ui_options.show_source = false;

  debug_stats_callbacks ui_callbacks;
  ui_callbacks.tid_push = [](thread_id tid) -> bool {
    std::stringstream ss;
    ss << "Thread " << tid;
    return ImGui::TreeNode(ss.str().c_str());
  };
  ui_callbacks.tid_pop = []() {ImGui::TreePop(); };
  ui_callbacks.node_push = [](const char* node_label) -> bool { return ImGui::TreeNode(node_label); };
  ui_callbacks.node_pop = []() {ImGui::TreePop(); };
  ui_callbacks.duration_push = [&ui_options](long long duration) {
    ImGui::SameLine();

    std::stringstream ss;
    ss << duration << " " << debug_stats_str(ui_options.unit);
    ImGui::Text(ss.str().c_str());
  };
  ui_callbacks.calls_push = [](unsigned long long count) {
    ImGui::SameLine();

    std::stringstream ss;
    ss << "(" << count << " call" << (count == 1 ? "" : "s") << " completed)";
    ImGui::Text(ss.str().c_str());
  };
  if (!hide_source) {
    ui_callbacks.file_line_push = [&file_snippet](const char* file, int line) {
      std::stringstream ss;
      ss << file << " @ " << line;
      if (ImGui::Button(ss.str().c_str())) {
        file_snippet->load_and_show(file, line);
      }
    };
  } else {
    ui_callbacks.file_line_push = nullptr;
  }

  auto frame_callback = [&stats, &ui_options, &ui_callbacks, &file_snippet, hide_source]() {

    TIMED_BLOCK("Frame callback");

    if (!is_recording_enabled()) {
      ImGui::Text("Event recording is not active!");
      ImGui::SameLine();
      if (ImGui::Button("Enable recording")) {
        enable_recording(1 << 20);
      }
      ImGui::Separator();
    }

    ImGui::Text("Event recording progress:");
    ImGui::ProgressBar(get_event_recording_progress());

    if (ImGui::Button("Take new snapshot")) {
      debug_stats_release(stats);
    }

    ImGui::Separator();

    if (stats == nullptr) {
      stats = debug_stats_get();
    }

    ImGui::Text("Timing:");
    ImGui::SameLine();
    if (ImGui::RadioButton("inclusive", ui_options.timing == debug_stats_timing::inclusive)) {
      ui_options.timing = debug_stats_timing::inclusive;
    }
    ImGui::SameLine();
    if (ImGui::RadioButton("exclusive", ui_options.timing == debug_stats_timing::exclusive)) {
      ui_options.timing = debug_stats_timing::exclusive;
    }

    ImGui::Text("Statistic:");
    ImGui::SameLine();
    if (ImGui::RadioButton("none", ui_options.stat_type == debug_stats_type::none)) {
      ui_options.stat_type = debug_stats_type::none;
    }
    ImGui::SameLine();
    if (ImGui::RadioButton("average", ui_options.stat_type == debug_stats_type::average)) {
      ui_options.stat_type = debug_stats_type::average;
    }
    ImGui::SameLine();
    if (ImGui::RadioButton("min", ui_options.stat_type == debug_stats_type::min)) {
      ui_options.stat_type = debug_stats_type::min;
    }
    ImGui::SameLine();
    if (ImGui::RadioButton("max", ui_options.stat_type == debug_stats_type::max)) {
      ui_options.stat_type = debug_stats_type::max;
    }

    ImGui::Text("Unit:");
    ImGui::SameLine();
    if (ImGui::RadioButton("nanoseconds", ui_options.unit == debug_stats_unit::nanoseconds)) {
      ui_options.unit = debug_stats_unit::nanoseconds;
    }
    ImGui::SameLine();
    if (ImGui::RadioButton("microseconds", ui_options.unit == debug_stats_unit::microseconds)) {
      ui_options.unit = debug_stats_unit::microseconds;
    }
    ImGui::SameLine();
    if (ImGui::RadioButton("milliseconds", ui_options.unit == debug_stats_unit::milliseconds)) {
      ui_options.unit = debug_stats_unit::milliseconds;
    }

    if (!hide_source) {
      ImGui::Text("Show source:");
      ImGui::SameLine();
      ImGui::Checkbox("", &ui_options.show_source);
    }

    ImGui::Separator();

    debug_stats_ui(stats, ui_options, ui_callbacks);

    ImGui::Separator();

    if (ImGui::Button("Export to XML")) {
      debug_stats_options xml_options;
      xml_options.timing = debug_stats_timing::inclusive;
      xml_options.stat_type = debug_stats_type::none;
      xml_options.unit = debug_stats_unit::nanoseconds;
      xml_options.show_source = !hide_source;

      debug_stats_xml(stats, xml_options, "stats.xml");
    }

    ImGui::SameLine();
    if (ImGui::Button("Export to Chrome tracing JSON")) {
      debug_stats_chrome_tracing(stats, "chrome_tracing.json", hide_source);
    }

    if (file_snippet) {
      file_snippet->render();
    }
  };

  using flags = imgui_app::window_options_flag;

  imgui_app::window_options main_window_flags = 0;
  if (options.force_topmost) {
    main_window_flags |= flags::force_topmost;
  }
  if (options.hide_from_taskbar) {
    main_window_flags |= flags::hide_from_taskbar;
  }

  const int imgui_app_result = imgui_app::main_window(title, 1024, 512, main_window_flags, frame_callback);

  debug_stats_release(stats);

  return imgui_app_result;
}

}
}
