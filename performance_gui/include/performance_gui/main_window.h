
#ifndef MAIN_WINDOW_H_INCLUDED_874445AC_7A7B_462C_A53B_BCBB9343A575
#define MAIN_WINDOW_H_INCLUDED_874445AC_7A7B_462C_A53B_BCBB9343A575

namespace performance {
namespace gui {

struct main_window_options
{
  bool force_topmost = false;
  bool hide_from_taskbar = false;

  bool hide_source = false;
};

int main_window(const char* title, const main_window_options& options);

}
}

#endif // MAIN_WINDOW_H_INCLUDED_874445AC_7A7B_462C_A53B_BCBB9343A575
