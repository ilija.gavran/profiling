
#include <performance/debug_event_buffer.h>

#include <performance/debug_event.h>

#include <cstring>

namespace performance {
namespace detail {

debug_event_buffer::debug_event_buffer(std::size_t event_count) :
  event_count_(event_count),
  idx_mask_(event_count - 1),
  events_(new debug_event[event_count]),
  next_event_idx_(0)
{
  PERFORMANCE_ASSERT_MSG((event_count & (event_count - 1)) == 0, "Event count must be a power of 2!");
  clear();
}

debug_event_buffer::~debug_event_buffer() = default;

void debug_event_buffer::record_event(const debug_event& event)
{
  std::shared_lock guard(buffer_mutex_);

  const auto event_idx = next_event_idx_.fetch_add(1, std::memory_order_relaxed) & idx_mask_;
  events_[event_idx] = event;
}

void debug_event_buffer::consume_events(std::function<void(const debug_event&)> callback)
{
  auto snapshot = std::unique_ptr<debug_event[]>(new debug_event[event_count_]);
  std::size_t snapshot_end;
  {
    std::lock_guard<std::shared_mutex> guard(buffer_mutex_);
    // take snapshot of event buffer
    std::copy(events_.get(), events_.get() + event_count_, snapshot.get());
    snapshot_end = next_event_idx_.load(std::memory_order_relaxed) & idx_mask_;
    // clear and reset the buffer
    clear();
    next_event_idx_.store(0, std::memory_order_relaxed);
  }

  // iterate over the snapshot
  auto snapshot_current = snapshot_end;
  do {
    const debug_event& current_event = snapshot[snapshot_current];
    if (current_event.block.file != nullptr) {
      callback(current_event);
    }
    snapshot_current = (snapshot_current + 1) & idx_mask_;
  } while (snapshot_current != snapshot_end);
}

float debug_event_buffer::get_progress() const
{
  const auto current_event = next_event_idx_.load(std::memory_order_relaxed) & idx_mask_;
  return current_event / static_cast<float>(event_count_);
}

void debug_event_buffer::clear()
{
  std::memset(static_cast<void*>(events_.get()), 0, sizeof(debug_event) * event_count_);
}

}
}

namespace {

std::mutex create_event_buffer_mutex;

std::atomic<performance::detail::debug_event_buffer*> event_buffer(nullptr);

}

namespace performance {

void create_event_buffer(std::size_t event_count)
{
  std::lock_guard<std::mutex> guard(create_event_buffer_mutex);

  if (event_buffer.load(std::memory_order_relaxed) != nullptr) {
    PERFORMANCE_ASSERT_MSG(false, "Event buffer can be created only once!");
    return;
  }

  event_buffer.store(new detail::debug_event_buffer(event_count), std::memory_order_release);
}

detail::debug_event_buffer* get_event_buffer()
{
  return event_buffer.load(std::memory_order_acquire);
}

}
