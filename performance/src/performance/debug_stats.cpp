
#include <performance/debug_stats.h>

#include <performance/base.h>
#include <performance/debug_block.h>
#include <performance/debug_event.h>
#include <performance/debug_event_buffer.h>

#include <cstring>
#include <algorithm>
#include <fstream>
#include <map>
#include <vector>

namespace performance {

// We can have only one debug block per line of code!
inline bool operator==(const debug_block& block_a, const debug_block& block_b)
{
  return (block_a.line == block_b.line && std::strcmp(block_a.file, block_b.file) == 0);
}

}

namespace {

using namespace performance;

using duration = clock::duration;

struct call_tree_node_durations
{
  duration sum = duration(0);
  duration min = duration::max();
  duration max = duration(0);
};

struct call_tree_node
{
  debug_block block = {};
  const debug_event* current_begin_event = nullptr;

  unsigned int hit_count = 0;

  call_tree_node_durations inclusive;
  call_tree_node_durations exclusive;

  call_tree_node* parent = nullptr;
  std::vector<std::unique_ptr<call_tree_node>> children;

  call_tree_node() = default;

  call_tree_node(const call_tree_node&) = delete;
  call_tree_node& operator=(const call_tree_node&) = delete;

  call_tree_node(call_tree_node&&) = default;
  call_tree_node& operator=(call_tree_node&&) = default;

  ~call_tree_node() = default;

  call_tree_node* add_child(const debug_block& child_block)
  {
    for (auto& child : children) {
      if (child->block == child_block) {
        return child.get();
      }
    }
    auto new_child = std::make_unique<call_tree_node>();
    new_child->block = child_block;
    new_child->parent = this;
    auto result = new_child.get();
    children.emplace_back(std::move(new_child));
    return result;
  }

  template<typename Callback>
  void traverse(Callback callback)
  {
    for (auto& child : children) {
      callback(child.get());
      child->traverse(callback);
    }
  }
};

using thread_events_t = std::map<thread_id, std::vector<debug_event>>;
using thread_call_trees_t = std::map<thread_id, std::vector<call_tree_node>>;

}

namespace performance {

struct debug_stats
{
  thread_events_t thread_events;
  thread_call_trees_t thread_call_trees;
};

}

namespace {

void analyze_events(const thread_events_t& thread_events, thread_call_trees_t& thread_call_trees)
{
  for (auto&[tid, events] : thread_events)
  {
    call_tree_node call_tree;
    call_tree_node* current_node = &call_tree;

    // build call tree
    for (auto& event : events) {
      if (event.type == debug_event_type::begin_block) {
        current_node = current_node->add_child(event.block);

        PERFORMANCE_ASSERT(current_node->current_begin_event == nullptr);
        current_node->current_begin_event = &event;
      } else {
        PERFORMANCE_ASSERT(event.type == debug_event_type::end_block);
        if (current_node->parent != nullptr) {
          PERFORMANCE_ASSERT(current_node->current_begin_event != nullptr);
          PERFORMANCE_ASSERT(current_node->block == event.block);

          current_node->hit_count += 1;

          const auto node_duration = event.time - current_node->current_begin_event->time;

          current_node->inclusive.sum += node_duration;
          current_node->inclusive.min = (std::min)(current_node->inclusive.min, node_duration);
          current_node->inclusive.max = (std::max)(current_node->inclusive.max, node_duration);

          current_node->current_begin_event = nullptr;
          current_node = current_node->parent;
        } else {
          // reset call tree
          current_node = &call_tree;
          call_tree.children.clear();
        }
      }
    }

    // calculate exclusive node times
    call_tree.traverse([](call_tree_node* node) {
      node->exclusive = node->inclusive;
    });
    call_tree.traverse([](call_tree_node* node) {
      if (node->parent != nullptr && node->hit_count > 0) {
        node->parent->exclusive.sum -= node->inclusive.sum;
        node->parent->exclusive.max -= node->inclusive.min;
        node->parent->exclusive.min -= node->inclusive.max;
      }
    });

    // TODO: we should probably separate stats of currently running stack

    thread_call_trees[tid].emplace_back(std::move(call_tree));
  }
}

long long debug_stats_duration(duration d, debug_stats_unit stat_unit)
{
  using namespace std::chrono;

  if (stat_unit == debug_stats_unit::nanoseconds) {
    return duration_cast<nanoseconds>(d).count();
  } else if (stat_unit == debug_stats_unit::microseconds) {
    return duration_cast<microseconds>(d).count();
  } else if (stat_unit == debug_stats_unit::milliseconds) {
    return duration_cast<milliseconds>(d).count();
  } else {
    PERFORMANCE_ASSERT(false);
    return -1;
  }
}

void debug_stats_rec(call_tree_node* node, const debug_stats_options& options, const debug_stats_callbacks& callbacks)
{
  if (callbacks.node_push(node->block.name)) {

    const call_tree_node_durations* node_durations = nullptr;
    if (options.timing == debug_stats_timing::inclusive) {
      node_durations = &node->inclusive;
    } else if (options.timing == debug_stats_timing::exclusive) {
      node_durations = &node->exclusive;
    } else {
      PERFORMANCE_ASSERT(false);
    }

    // if call to this node is not completed, force inclusive times
    if (node->hit_count == 0) {
      node_durations = &node->inclusive;
    }

    duration node_duration = duration(0);
    if (options.stat_type == debug_stats_type::none) {
      node_duration = node_durations->sum;
    } else if (node->hit_count > 0) {
      if (options.stat_type == debug_stats_type::average) {
        node_duration = node_durations->sum / node->hit_count;
      } else if (options.stat_type == debug_stats_type::min) {
        node_duration = node_durations->min;
      } else if (options.stat_type == debug_stats_type::max) {
        node_duration = node_durations->max;
      } else {
        PERFORMANCE_ASSERT(false);
      }
    }

    callbacks.duration_push(debug_stats_duration(node_duration, options.unit));

    callbacks.calls_push(node->hit_count);

    if (options.show_source) {
      callbacks.file_line_push(node->block.file, node->block.line);
    }

    for (auto& child : node->children) {
      debug_stats_rec(child.get(), options, callbacks);
    }

    callbacks.node_pop();
  }
}

void debug_stats_traverse(debug_stats* stats, debug_stats_options options, const debug_stats_callbacks& callbacks)
{
  for (auto&[tid, call_trees] : stats->thread_call_trees) {
    if (callbacks.tid_push(tid)) {
      for (auto& call_tree : call_trees) {
        for (auto& child : call_tree.children) {
          debug_stats_rec(child.get(), options, callbacks);
        }
      }
      callbacks.tid_pop();
    }
  }
}

std::string json_escape(std::string_view sv)
{
  std::string result;
  result.reserve(sv.size() * 2);

  for (char ch : sv) {
    if (ch == '\\') {
      result.append(2, '\\');
    } else {
      result += ch;
    }
  }

  return result;
}

}

namespace performance {

debug_stats* debug_stats_get()
{
  auto result = new debug_stats;

  auto event_buffer = get_event_buffer();
  if (event_buffer != nullptr) {
    event_buffer->consume_events([&result](const debug_event& event) {
      result->thread_events[event.tid].push_back(event);
    });

    analyze_events(result->thread_events, result->thread_call_trees);
  }

  return result;
}

void debug_stats_ui(debug_stats* stats, debug_stats_options options, const debug_stats_callbacks& callbacks)
{
  debug_stats_traverse(stats, options, callbacks);
}

bool debug_stats_xml(debug_stats* stats, debug_stats_options options, const char* file_path)
{
  std::ofstream ofs(file_path);
  if (!ofs.is_open()) {
    return false;
  }

  debug_stats_callbacks callbacks;
  callbacks.tid_push = [&ofs](thread_id tid) {
    ofs << "<thread id=\"" << tid << "\">\n";
    return true;
  };
  callbacks.tid_pop = [&ofs]() {ofs << "</thread>\n"; };
  callbacks.node_push = [&ofs](const char* node_label) {
    ofs << "<node name=\"" << node_label << "\" ";
    return true;
  };
  callbacks.node_pop = [&ofs]() {ofs << "</node>\n"; };
  callbacks.duration_push = [&ofs](long long duration) {
    ofs << "duration=\"" << duration << "\" ";
  };
  callbacks.calls_push = [&ofs](unsigned long long count) {
    ofs << "calls=\"" << count << "\">\n";
  };
  callbacks.file_line_push = [&ofs](const char* file, int line) {
    ofs << "<source file=\"" << file <<  "\" line=\"" << line << "\" />\n";
  };

  ofs << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  ofs << "<stats timing=\"" << debug_stats_str(options.timing)
    << "\" type=\"" << debug_stats_str(options.stat_type)
    << "\" unit=\"" << debug_stats_str(options.unit) << "\">\n";
  debug_stats_traverse(stats, options, callbacks);
  ofs << "</stats>\n";

  return true;
}

bool debug_stats_chrome_tracing(debug_stats* stats, const char* file_path, bool hide_source)
{
  std::ofstream ofs(file_path);
  if (!ofs.is_open()) {
    return false;
  }

  ofs << "{\"traceEvents\":[\n";

  auto& thread_events = stats->thread_events;

  bool first_event = true;

  for (auto&[tid, events] : thread_events) {
    for (auto& event : events) {
      const char* ph = (event.type == debug_event_type::begin_block ? "B" : "E");
      auto ts = debug_stats_duration(event.time.time_since_epoch(), debug_stats_unit::microseconds);

      if (!first_event) {
        ofs << ",\n";
      }

      ofs << "{";
      ofs << "\"args\":";
      {
        ofs << "{";
        if (!hide_source) {
          ofs << "\"src_file\":\"" << json_escape(event.block.file).c_str() << "\",";
          ofs << "\"src_line\":\"" << event.block.line << "\"";
        }
        ofs << "},";
      }
      ofs << "\"cat\":\"timed_block\",";
      ofs << "\"name\":\"" << json_escape(event.block.name).c_str() << "\",";
      ofs << "\"ph\":\"" << ph << "\",";
      ofs << "\"pid\":\"0\",";
      ofs << "\"tid\":\"" << tid << "\",";
      ofs << "\"ts\":\"" << ts << "\"";
      ofs << "}";

      first_event = false;
    }
  }

  ofs << "]}\n";

  return true;
}

void debug_stats_release(debug_stats*& stats)
{
  if (stats != nullptr) {
    delete stats;
    stats = nullptr;
  }
}

float get_event_recording_progress()
{
  auto event_buffer = get_event_buffer();
  if (event_buffer != nullptr) {
    return event_buffer->get_progress();
  }
  return 0.f;
}

}
