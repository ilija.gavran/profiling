
#ifndef DEBUG_EVENT_BUFFER_H_INCLUDED_19274802_BAE4_481E_9ED5_9BCB4297C63B
#define DEBUG_EVENT_BUFFER_H_INCLUDED_19274802_BAE4_481E_9ED5_9BCB4297C63B

#include <atomic>
#include <cstdint>
#include <functional>
#include <memory>
#include <shared_mutex>

namespace performance {

struct debug_event;

namespace detail {

class debug_event_buffer
{
public:
  explicit debug_event_buffer(std::size_t event_count);
  debug_event_buffer(const debug_event_buffer&) = delete;
  debug_event_buffer& operator=(const debug_event_buffer&) = delete;
  ~debug_event_buffer();

public:
  void record_event(const debug_event& event);

  void consume_events(std::function<void(const debug_event&)> callback);

  float get_progress() const;

private:
  void clear();

private:
  const std::size_t event_count_;
  const std::size_t idx_mask_;
  std::unique_ptr<debug_event[]> events_;
  std::atomic<std::size_t> next_event_idx_;
  mutable std::shared_mutex buffer_mutex_;
};

}

void create_event_buffer(std::size_t event_count);

detail::debug_event_buffer* get_event_buffer();

}

#endif // DEBUG_EVENT_BUFFER_H_INCLUDED_19274802_BAE4_481E_9ED5_9BCB4297C63B
