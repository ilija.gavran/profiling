
#include <performance/base.h>

#include <cstdio>
#include <stdexcept>

namespace performance {
namespace detail {

void failed_assert(const char* condition, const char* function, const char* file, int line)
{
  char buffer[1024];

  std::snprintf(buffer, sizeof(buffer) - 1,
    "Assertion failed: %s (%s in %s@%d)", condition, function, file, line);
  buffer[sizeof(buffer) - 1] = '\0';

  std::fputs(buffer, stderr);

  throw std::runtime_error(buffer);
}

}
}
