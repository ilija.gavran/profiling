
#include <performance/profiler.h>

#include <performance/debug_event_buffer.h>

namespace performance {

bool is_recording_enabled()
{
  auto event_buffer = get_event_buffer();
  return (event_buffer != nullptr);
}

void enable_recording(std::size_t event_count)
{
  create_event_buffer(event_count);
}

void record_event(const debug_event& event)
{
  auto event_buffer = get_event_buffer();
  if (event_buffer != nullptr) {
    event_buffer->record_event(event);
  }
}

}
