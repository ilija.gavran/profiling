
#ifndef DEBUG_BLOCK_H_INCLUDED_0E99FE0B_FED1_440B_9F54_B431CD23701F
#define DEBUG_BLOCK_H_INCLUDED_0E99FE0B_FED1_440B_9F54_B431CD23701F

namespace performance {

struct debug_block
{
  const char* name;
  const char* file;
  int line;
};

}

#endif // DEBUG_BLOCK_H_INCLUDED_0E99FE0B_FED1_440B_9F54_B431CD23701F
