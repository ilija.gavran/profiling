
#ifndef PROFILER_H_INCLUDED_F2A3346A_127A_41E2_AF0A_4D5001596DC7
#define PROFILER_H_INCLUDED_F2A3346A_127A_41E2_AF0A_4D5001596DC7

#include <performance/base.h>
#include <performance/debug_block.h>
#include <performance/debug_event.h>

namespace performance {

bool is_recording_enabled();

void enable_recording(std::size_t event_count);

void record_event(const debug_event& event);

class timed_block
{
public:
  timed_block(const char* block_name, const char* file, int line) :
    block_{ block_name, file, line },
    tid_(std::this_thread::get_id())
  {
    record_event({ debug_event_type::begin_block, block_, tid_, clock::now() });
  }

  ~timed_block()
  {
    record_event({ debug_event_type::end_block, block_, tid_, clock::now() });
  }

private:
  const debug_block block_;
  const thread_id tid_;
};

}

#if PERF_PROFILER
#define TIMED_BLOCK_VAR_(x, y) x ## y
#define TIMED_BLOCK_(block_name, counter) \
  performance::timed_block TIMED_BLOCK_VAR_(_timed_block_, counter) (block_name, __FILE__, __LINE__)
#define TIMED_BLOCK(block_name) TIMED_BLOCK_(block_name, __COUNTER__)
#define TIMED_FUNCTION TIMED_BLOCK(__FUNCTION__)
#else
#define TIMED_BLOCK(block_name) (void)0
#define TIMED_FUNCTION (void)0
#endif

#endif // PROFILER_H_INCLUDED_F2A3346A_127A_41E2_AF0A_4D5001596DC7
