
#ifndef DEBUG_EVENT_H_INCLUDED_326D36AD_219E_4A65_9B20_D615DC0AF8EA
#define DEBUG_EVENT_H_INCLUDED_326D36AD_219E_4A65_9B20_D615DC0AF8EA

#include <performance/base.h>
#include <performance/debug_block.h>

namespace performance {

enum class debug_event_type
{
  begin_block,
  end_block
};

struct debug_event
{
  debug_event_type type;
  debug_block block;
  thread_id tid;
  time_point time;
};

}

#endif // DEBUG_EVENT_H_INCLUDED_326D36AD_219E_4A65_9B20_D615DC0AF8EA
