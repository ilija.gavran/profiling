
#ifndef BASE_H_INCLUDED_1DD6D3B8_706E_4E72_B339_07C8F969B2DC
#define BASE_H_INCLUDED_1DD6D3B8_706E_4E72_B339_07C8F969B2DC

#include <chrono>
#include <thread>

namespace performance {

using clock = std::chrono::steady_clock;
using time_point = clock::time_point;
using thread_id = std::thread::id;

namespace detail {

void failed_assert(const char* condition, const char* function, const char* file, int line);

}

}

#ifndef NDEBUG
#define PERFORMANCE_ASSERT_MSG(x, msg) \
if (!(x)) {\
  performance::detail::failed_assert(msg, __FUNCTION__, __FILE__, __LINE__);\
} (void)0
#else
#define PERFORMANCE_ASSERT_MSG(x, msg) (void)0
#endif

#define PERFORMANCE_ASSERT(x) PERFORMANCE_ASSERT_MSG(x, #x)

#endif // BASE_H_INCLUDED_1DD6D3B8_706E_4E72_B339_07C8F969B2DC
