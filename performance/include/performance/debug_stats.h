
#ifndef DEBUG_STATS_H_INCLUDED_ADF6508F_A448_4A75_80FB_E5ED557D71E0
#define DEBUG_STATS_H_INCLUDED_ADF6508F_A448_4A75_80FB_E5ED557D71E0

#include <performance/base.h>

#include <functional>

namespace performance {

enum class debug_stats_timing
{
  inclusive,
  exclusive
};

inline const char* debug_stats_str(debug_stats_timing timing)
{
  if (timing == debug_stats_timing::inclusive) {
    return "inclusive";
  } else if (timing == debug_stats_timing::exclusive) {
    return "exclusive";
  } else {
    PERFORMANCE_ASSERT(false);
    return "unknown_timing";
  }
}

enum class debug_stats_type
{
  none,
  average,
  min,
  max
};

inline const char* debug_stats_str(debug_stats_type stat_type)
{
  if (stat_type == debug_stats_type::none) {
    return "";
  } else if (stat_type == debug_stats_type::average) {
    return "average";
  } else if (stat_type == debug_stats_type::min) {
    return "min";
  } else if (stat_type == debug_stats_type::max) {
    return "max";
  } else {
    PERFORMANCE_ASSERT(false);
    return "unknown_stat_type";
  }
}

enum class debug_stats_unit
{
  nanoseconds,
  microseconds,
  milliseconds
};

inline const char* debug_stats_str(debug_stats_unit unit)
{
  if (unit == debug_stats_unit::nanoseconds) {
    return "ns";
  } else if (unit == debug_stats_unit::microseconds) {
    return "us";
  } else if (unit == debug_stats_unit::milliseconds) {
    return "ms";
  }
  PERFORMANCE_ASSERT(false);
  return "unknown_unit";
}

struct debug_stats_options
{
  debug_stats_timing timing;
  debug_stats_type stat_type;
  debug_stats_unit unit;
  bool show_source;
};

struct debug_stats_callbacks
{
  using debug_stats_tid_push = std::function<bool(thread_id tid)>;
  using debug_stats_tid_pop = std::function<void(void)>;
  using debug_stats_node_push = std::function<bool(const char* node_label)>;
  using debug_stats_node_pop = std::function<void(void)>;
  using debug_stats_duration_push = std::function<void(long long duration)>;
  using debug_stats_calls_push = std::function<void(unsigned long long count)>;
  using debug_stats_file_line_push = std::function<void(const char* file, int line)>;

  debug_stats_tid_push tid_push;
  debug_stats_tid_pop tid_pop;
  debug_stats_node_push node_push;
  debug_stats_node_pop node_pop;
  debug_stats_duration_push duration_push;
  debug_stats_calls_push calls_push;
  debug_stats_file_line_push file_line_push;
};

struct debug_stats;

debug_stats* debug_stats_get();
void debug_stats_ui(debug_stats* stats, debug_stats_options options, const debug_stats_callbacks& callbacks);
bool debug_stats_xml(debug_stats* stats, debug_stats_options options, const char* file_path);
bool debug_stats_chrome_tracing(debug_stats* stats, const char* file_path, bool hide_source);
void debug_stats_release(debug_stats*& stats);

float get_event_recording_progress();

}

#endif // DEBUG_STATS_H_INCLUDED_ADF6508F_A448_4A75_80FB_E5ED557D71E0
