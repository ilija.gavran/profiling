
#include <imgui_win32dx11/imgui_app.h>

#include <imgui.h>
#include <examples/imgui_impl_win32.h>
#include <examples/imgui_impl_dx11.h>
#include <d3d11.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <tchar.h>
#include <Windows.h>
#include <windowsx.h>

// Data
static ID3D11Device*            g_pd3dDevice = NULL;
static ID3D11DeviceContext*     g_pd3dDeviceContext = NULL;
static IDXGISwapChain*          g_pSwapChain = NULL;
static ID3D11RenderTargetView*  g_mainRenderTargetView = NULL;

// Forward declarations of helper functions
static bool CreateDeviceD3D(HWND hWnd);
static void CleanupDeviceD3D();
static void CreateRenderTarget();
static void CleanupRenderTarget();
static LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

namespace {

int window_width = 0;
int window_height = 0;

const int window_border_thickness = 3;

const char* global_title = nullptr;
bool* global_running = nullptr;
imgui_app::frame_callback_t* global_frame_callback = nullptr;

template<typename T>
class ScopedTempValue
{
public:
  explicit ScopedTempValue(T* object, const T& new_value) :
    object(object),
    prev_value(*object)
  {
    *object = new_value;
  }
  ~ScopedTempValue()
  {
    *object = prev_value;
  }
private:
  T* object;
  const T prev_value;
};

void RenderOneFrame()
{
  {

    // Start the Dear ImGui frame
    ImGui_ImplDX11_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();

    ImGui::SetNextWindowPos(ImVec2(0, 0));
    ImGui::SetNextWindowSize(ImVec2((float)window_width, (float)window_height));
    {
      ScopedTempValue<float> temp_value(&ImGui::GetStyle().WindowRounding, 0.f);
      ImGui::Begin(global_title, global_running, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoBringToFrontOnFocus);
    }

    (*global_frame_callback)();

    ImGui::End();
  }

  {

    const ImVec4 clear_color = ImVec4(0.f, 0.f, 0.f, 1.f);

    // Rendering
    ImGui::Render();
    g_pd3dDeviceContext->OMSetRenderTargets(1, &g_mainRenderTargetView, NULL);
    g_pd3dDeviceContext->ClearRenderTargetView(g_mainRenderTargetView, (float*)&clear_color);

    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
  }
}

}

namespace imgui_app {

int main_window(const char* title, int width, int height, window_options options, frame_callback_t frame_callback)
{
  bool running = true;

  window_width = width;
  window_height = height;

  global_title = title;
  global_running = &running;
  global_frame_callback = &frame_callback;

  // Create application window
  WNDCLASSEX wc = { sizeof(WNDCLASSEX), 0, WndProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, _T("ImGui"), NULL };
  ::RegisterClassEx(&wc);

  DWORD dwExStyle = 0;
  if (options & window_options_flag::hide_from_taskbar) {
    dwExStyle |= WS_EX_TOOLWINDOW;
  } else {
    dwExStyle |= WS_EX_APPWINDOW;
  }
  if (options & window_options_flag::force_topmost) {
    dwExStyle |= WS_EX_TOPMOST;
  }

  HWND hwnd = ::CreateWindowExA(dwExStyle, wc.lpszClassName, "", WS_POPUP, 100, 100, width, height, NULL, NULL, wc.hInstance, NULL);

  // Initialize Direct3D
  if (!CreateDeviceD3D(hwnd)) {
    CleanupDeviceD3D();
    ::UnregisterClass(wc.lpszClassName, wc.hInstance);
    return 1;
  }

  // Show the window
  ::ShowWindow(hwnd, SW_SHOW);
  ::UpdateWindow(hwnd);

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO(); (void)io;
  //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
  //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

  // Setup Dear ImGui style
  ImGui::StyleColorsDark();
  //ImGui::StyleColorsClassic();

  // sRGB -> linear space
  for (int i = 0; i < ImGuiCol_COUNT; ++i) {
    auto color = ImGui::GetStyle().Colors + i;
    color->x *= color->x;
    color->y *= color->y;
    color->z *= color->z;
  }

  // Setup Platform/Renderer bindings
  ImGui_ImplWin32_Init(hwnd);
  ImGui_ImplDX11_Init(g_pd3dDevice, g_pd3dDeviceContext);

  // Main loop
  MSG msg;
  ZeroMemory(&msg, sizeof(msg));
  for (;;) {
    // Poll and handle messages (inputs, window resize, etc.)
    // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
    // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
    // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
    // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
    while (::PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE) > 0) {
      ::TranslateMessage(&msg);
      ::DispatchMessage(&msg);
    }

    if (!running) {
      break;
    }

    RenderOneFrame();

    {
      g_pSwapChain->Present(1, 0); // Present with vsync
      //g_pSwapChain->Present(0, 0); // Present without vsync
    }
  }

  // Cleanup
  ImGui_ImplDX11_Shutdown();
  ImGui_ImplWin32_Shutdown();
  ImGui::DestroyContext();

  CleanupDeviceD3D();
  ::DestroyWindow(hwnd);
  ::UnregisterClass(wc.lpszClassName, wc.hInstance);

  return 0;
}

}

// Helper functions

static bool CreateDeviceD3D(HWND hWnd)
{
  // Setup swap chain
  DXGI_SWAP_CHAIN_DESC sd;
  ZeroMemory(&sd, sizeof(sd));
  sd.BufferCount = 2;
  sd.BufferDesc.Width = 0;
  sd.BufferDesc.Height = 0;
  sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
  sd.BufferDesc.RefreshRate.Numerator = 60;
  sd.BufferDesc.RefreshRate.Denominator = 1;
  sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
  sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
  sd.OutputWindow = hWnd;
  sd.SampleDesc.Count = 1;
  sd.SampleDesc.Quality = 0;
  sd.Windowed = TRUE;
  sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

  UINT createDeviceFlags = 0;
  //createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
  D3D_FEATURE_LEVEL featureLevel;
  const D3D_FEATURE_LEVEL featureLevelArray[2] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_0, };
  if (D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, featureLevelArray, 2, D3D11_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice, &featureLevel, &g_pd3dDeviceContext) != S_OK)
    return false;

  CreateRenderTarget();
  return true;
}

static void CleanupDeviceD3D()
{
  CleanupRenderTarget();
  if (g_pSwapChain) { g_pSwapChain->Release(); g_pSwapChain = NULL; }
  if (g_pd3dDeviceContext) { g_pd3dDeviceContext->Release(); g_pd3dDeviceContext = NULL; }
  if (g_pd3dDevice) { g_pd3dDevice->Release(); g_pd3dDevice = NULL; }
}

static void CreateRenderTarget()
{
  ID3D11Texture2D* pBackBuffer;
  g_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pBackBuffer));
  g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &g_mainRenderTargetView);
  pBackBuffer->Release();
}

static void CleanupRenderTarget()
{
  if (g_mainRenderTargetView) { g_mainRenderTargetView->Release(); g_mainRenderTargetView = NULL; }
}

// Win32 message handler
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
static LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
    return true;

  switch (msg) {
  case WM_GETMINMAXINFO:
  {
    auto min_max_info = reinterpret_cast<LPMINMAXINFO>(lParam);
    min_max_info->ptMinTrackSize.x = 100;
    min_max_info->ptMinTrackSize.y = 100;
  } return 0;
  case WM_SIZE:
    if (wParam != SIZE_MINIMIZED) {
      window_width = LOWORD(lParam);
      window_height = HIWORD(lParam);

      if (g_pSwapChain != nullptr) {
        CleanupRenderTarget();
        g_pSwapChain->ResizeBuffers(0, window_width, window_height, DXGI_FORMAT_UNKNOWN, 0);
        CreateRenderTarget();

        RenderOneFrame();

        g_pSwapChain->Present(0, 0);
      }
    }
    return 0;
  case WM_NCHITTEST:
  {
    POINT point;
    point.x = GET_X_LPARAM(lParam);
    point.y = GET_Y_LPARAM(lParam);
    ::ScreenToClient(hWnd, &point);
    
    const bool near_left = std::abs(point.x) <= window_border_thickness;
    const bool near_top = std::abs(point.y) <= window_border_thickness;
    const bool near_right = std::abs(point.x - window_width) <= window_border_thickness;
    const bool near_bottom = std::abs(point.y - window_height) <= window_border_thickness;

    if (near_left) {
      if (near_top) {
        return HTTOPLEFT;
      } else if (near_bottom) {
        return HTBOTTOMLEFT;
      } else {
        return HTLEFT;
      }
    } else if (near_right) {
      if (near_top) {
        return HTTOPRIGHT;
      } else if (near_bottom) {
        return HTBOTTOMRIGHT;
      } else {
        return HTRIGHT;
      }
    }
    if (near_top) {
      return HTTOP;
    } else if (near_bottom) {
      return HTBOTTOM;
    }

    // TODO: these constants are estimated, prove correctness
    const int titlebar_height = 20;
    const int close_button_width = 20;

    if (point.x > 0 && point.y > 0 && point.x < window_width - close_button_width && point.y < titlebar_height) {
      return HTCAPTION;
    }
  } break;
  case WM_SYSCOMMAND:
    if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
      return 0;
    break;
  case WM_CLOSE:
  case WM_DESTROY:
  case WM_QUIT:
    *global_running = false;
    break;
  }
  return ::DefWindowProc(hWnd, msg, wParam, lParam);
}
