
#ifndef IMGUI_APP_H_INCLUDED_9C668304_4589_4179_BECD_E454129ACC15
#define IMGUI_APP_H_INCLUDED_9C668304_4589_4179_BECD_E454129ACC15

#include <functional>

namespace imgui_app {

using frame_callback_t = std::function<void(void)>;

using uint = unsigned int;

struct window_options_flag
{
  static constexpr uint hide_from_taskbar = (1 << 0);
  static constexpr uint force_topmost = (1 << 1);
};

// combination of window_options_flag flags
using window_options = uint;

// ImGui main app window loop.
int main_window(const char* title, int width, int height, window_options options, frame_callback_t frame_callback);

}

#endif // IMGUI_APP_H_INCLUDED_9C668304_4589_4179_BECD_E454129ACC15
